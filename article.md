---
title: "Sobotkova vláda s koncem Šlachty překreslila bezpečnostní mapu země"
perex: "Něco přes dva roky trvalo vládě Bohuslava Sobotky, než obměnila vedení zásadních bezpečnostních složek. Padl bývalý policejní prezident Petr Lessy, jeho náměstek pro kriminální policii Václav Kučera a z vedení protikorupčního útvaru se poroučel Milan Komárek."
description: "S odchodem šéfa ÚOOZ Roberta Šlachty zkoumáme, jak často se mění vedoucí funkcionáři bezpečnostních sborů."
authors: ["Jan Cibulka", "Ondřej Franta", "Marcel Šulek"]
published: "20. června 2016"
coverimg: https://interaktivni.rozhlas.cz/vymeny-v-policii/media/urnak.jpg
url: "vymeny-v-policii"
libraries: [d3]
---

Šéfa pak vyměnila i vojenská a civilní rozvědka a rovněž skončil ředitel Generální inspekce bezpečnostních sborů Ivan Bílek. Nyní odchází i šéf útvaru pro odhalování organizovaného zločinu Robert Šlachta, který byl ve funkci od roku 2008.

Nejde ale jen o doménu Sobotkovy vlády, podobně si počínali i jeho předchůdci v posledních deseti letech. Špičky policie obměňovali jako premiéři i Jiří Rusnok, Petr Nečas i Mirek Topolánek. Jen vláda Jana Fischera nechala ve vedení bezpečnostních složek lidi, které „zdědila“ od té Topolánkovy.

Když byl v roce 2012 zbaven funkce tehdejší policejní prezident Petr Lessy, označil to opoziční poslanec ČSSD Jeroným Tejc za snahu o ovládnutí policie a *„policejní puč v režii pravicové vlády“*. O snaze opanovat bezpečnostní sbor mluví nyní i ministr vnitra Milan Chovanec, který v narážce na mocenské zájmy vicepremiéra Andreje Babiše prohlásil, že *„se policie nikdy nestane divizí Agrofertu“*.

*Výměny ve vedení důležitých bezpečnostních složek můžete prozkoumat v následující vizualizaci. Barevný kroužek okolo fotografie ukazuje, za kterých premiérů dotyčný sloužil.*

<aside class="big">
  <div class="ig" data-ig="base"></div>
</aside>

## Nejostřelovanější židle? Šéf protikorupčníků

Miloslav Brych, Renata Strnadová, Jiří Novák, Libor Vrba, Tomáš Martinec, Milan Komárek a Jaroslav Vild, ti všichni se od roku 2005 vystřídali v křesle vedoucího útvaru odhalování korupce a finanční kriminality. Dá se tedy říct, že z posledních pěti premiérů měl každý „svého“ protikorupčního šéfa.

Fluktuace ve vedení protikorupční policie, kde jeden vedoucí vydrží v průměru rok, vynikne ve srovnání s dalšími celostátními speciály: Nyní odcházející šéf bojovníků s organizovaným zločinem Robert Šlachta nastoupil do funkce v roce 2008 za vlády Mirka Topolánka, vystřídal tak Jana Kubiceho, který útvar vedl od roku 2005.

Stabilní je i protidrogová centrála, té vládl od roku 2005 Jiří Komorous, kterého v roce 2009 nahradil současný šéf Jakub Frydrych. Tři šéfy pak od roku 2000 zažila cizinecká policie.

Pouze jediný policejní prezident za posledních deset let, konkrétně Oldřich Martinů, zažil na svém místě více jak dva premiéry. Nejkratší angažmá, tedy roční, potkalo Petra Lessyho, a to dokonce dvakrát. Policii vedl za vlády Petra Nečase a následně za Jiřího Rusnoka.

Podobně rychle jako šéfové policie se střídají i velitelé vojenského zpravodajství. Vojenské špiony vede od roku 2014 Jan Beroun, který přišel s premiérem Bohuslavem Sobotkou. Mezi jeho předchůdce patřili i Ondrej Páleník a Milan Kovanda, kteří se zapletli do kauzy okolo Petra Nečase a Jany Nagyové.

O něco pomalejší tempo panuje ve výměnách vedoucích civilní rozvědky, nynější Jiří Šašek vystřídal po sedmi letech funkce Ivo Schwarze. Poměrně stabilní bylo i vedení „hlídačů policie“, tedy Generální inspekce bezpečnostních sborů. Tam koncem loňského roku po čtyřech letech skončil Ivan Bílek, tomu zlomila vaz údajná nečinnost inspekce při vyšetřování klientelismu v policii Olomouckého kraje.
