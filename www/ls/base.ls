months1 = <[leden únor březen duben květen červen červenec srpen září říjen listopad prosinec]>
months2 = <[ledna února března dubna května června července srpna září října listopadu prosince]>

toDateFull = ->
  date = new Date!
  if it
    [day, month, year] = it.split /[\. ]+/
    day = parseInt day
    monthNum = months2.indexOf month
    year = parseInt year
    date
      ..setTime 12 * 3600 * 1e3
      ..setFullYear year
      ..setMonth monthNum
      ..setDate day
  date

toDateMonth = ->
  date = new Date!
  if it.length > 2
    [month, year] = it.split /[\. ]+/
    monthNum = months1.indexOf month
    year = parseInt year
    date
      ..setTime 12 * 3600 * 1e3
      ..setFullYear year
      ..setMonth monthNum
  date

toShortName = ->
  [name, surname] = it.split " "
  "#{name[0]}. #surname"

class Color
  (@color, @startPercent, @endPercent) ->

class TreeObject
  (@title, @people) ->
    @people ?= []
    @children = []

  addPerson: ->
    @people.push it

  addChild: (treeObject) ->
    @children.push treeObject

premieri = d3.tsv.parse ig.data.premieri, (row) ->
  row.start = toDateFull row.nastup
  row.end = toDateFull row.konec
  row.name = row.jmeno
  row.colors = [new Color row.barva, 0, 1]
  row

getColors = (startDate, endDate) ->
  colors = []
  duration = endDate.getTime! - startDate.getTime!
  for premier in premieri
    if premier.start < endDate and startDate < premier.end
      startPercent = (premier.start.getTime! - startDate.getTime!) / duration
      startPercent = Math.max startPercent, 0
      endPercent = (premier.end.getTime! - startDate.getTime!) / duration
      endPercent = Math.min endPercent, 1
      colors.push new Color premier.barva, startPercent, endPercent
  colors

root = new TreeObject "Premiér", premieri

uradyAssoc = {}
policajti = d3.tsv.parse ig.data.policajti, (row) ->
  row.start = toDateMonth row.od
  row.end = toDateMonth row.do
  row.colors = getColors row.start, row.end
  row.name = toShortName row.jmeno
  uradyAssoc[row.pozice] ?= new TreeObject row.pozice
  uradyAssoc[row.pozice].addPerson row
  row

root
  ..addChild uradyAssoc['Policejní prezident']
  ..addChild uradyAssoc['Vojenské zpravodajství']
  ..addChild uradyAssoc['Bezpečnostní informační služba']
  ..addChild uradyAssoc['Úřad pro zahraniční styky a informace']
  ..addChild uradyAssoc['Generální inspekce bezpečnostních sborů']

uradyAssoc['Policejní prezident']
  ..addChild uradyAssoc['První náměstek policejního prezidenta']
  ..addChild uradyAssoc['Náměstek policejního prezidenta pro SKPV']

uradyAssoc['První náměstek policejního prezidenta']
  ..addChild uradyAssoc['Služba cizinecké policie']

uradyAssoc['Náměstek policejního prezidenta pro SKPV']
  ..addChild uradyAssoc['Útvar odhalování korupce a finanční kriminality SKPV']
  ..addChild uradyAssoc['Útvar pro odhalování organizovaného zločinu']
  ..addChild uradyAssoc['Národní protidrogová centrála SKPV']

uradyAssoc['Bezpečnostní informační služba'].title = "Bezpečnostní<br>informační služba"
uradyAssoc['Úřad pro zahraniční styky a informace'].title = "Úřad pro zahraniční<br>styky a informace"
uradyAssoc['Generální inspekce bezpečnostních sborů'].title = "Generální inspekce<br>bezpečnostních sborů"

uradyAssoc['První náměstek policejního prezidenta'].title = "První náměstek<br>policejního prezidenta"
uradyAssoc['Náměstek policejního prezidenta pro SKPV'].title = "Náměstek policejního<br>prezidenta pro SKPV"

uradyAssoc['Služba cizinecké policie'].title = "Služba cizinecké<br>policie"
uradyAssoc['Útvar odhalování korupce a finanční kriminality SKPV'].title = "Útvar odhalování korupce<br>a finanční kriminality SKPV"

uradyAssoc['Útvar pro odhalování organizovaného zločinu'].title = 'Útvar pro odhalování<br>organizovaného zločinu'
uradyAssoc['Útvar odhalování korupce a finanční kriminality SKPV'].title = 'Útvar odhalování korupce<br>a finanční kriminality SKPV'
uradyAssoc['Národní protidrogová centrála SKPV'].title = 'Národní protidrogová centrála SKPV'

imgSize = 52
imgBorderSize = 62
arc = d3.svg.arc!
  ..startAngle -> it.startPercent * 2 * Math.PI
  ..endAngle -> it.endPercent * 2 * Math.PI
  ..innerRadius imgSize / 2
  ..outerRadius imgBorderSize / 2

onHeadClicked = ->
  d3.event.preventDefault!
  duration = it.end.getTime! - it.start.getTime!
  mid = new Date!
    ..setTime it.start.getTime! + 0.5 * duration

  updateHeadActivity mid, mid

drawTree = (container, treeObjects, level) ->
  width = 100 / treeObjects.length
  zindex = 999
  htmlObjects = container.selectAll \div.tree-object .data treeObjects .enter!append \div
    ..attr \class "tree-object level-#level"
    ..style \width "#{width}%"
    ..append \div
      ..attr \class \heads
      ..selectAll \div.head-container .data (.people) .enter!append \div
        ..attr \class \head-container
        ..append \a
          ..attr \href \#
          ..on \click onHeadClicked
          ..attr \class \head
          ..style \z-index -> zindex--
          ..append \div
            ..attr \class \img
            ..each ->
              return unless it.foto
              d3.select @ .append \img .attr \src "media/square/#{it.foto}.jpg"
          ..append \svg
            ..attr \width imgBorderSize
            ..attr \height imgBorderSize
            ..selectAll \path .data (.colors) .enter!append \path
              ..attr \d arc
              ..attr \transform "translate(#{imgBorderSize / 2}, #{imgBorderSize / 2})"
              ..attr \fill (.color)
          ..append \span
            ..attr \class \name
            ..html (.name)
          ..append \span
            ..attr \class \year
            ..html ->
              st = it.start.getFullYear!
              en = it.end.getFullYear!
              if st == en
                st
              else
                "#st – #en"
      ..append \div
        ..attr \class \none
        ..html "K této pozici nemáme takto stará data"
      ..append \span
        ..attr \class \title
        ..html (.title)
    ..each ->
      count = it.children.length
      return unless count
      ele = d3.select @
      children = ele.append \div
        ..attr \class "children level-#level count-#count"
      drawTree do
        children
        it.children
        level + 1

base = d3.select ig.containers.base
container = base.append \div
  ..attr \class \container

drawTree container, [root], 0

heads = container.selectAll \.head-container

end = start = new Date!
  ..setDate 1

updateHeadActivity = (start, end) ->
  heads.classed \active ->
    start < it.end and it.start < end

updateHeadActivity start, end
